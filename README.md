# רשימות קורסים חופשיות

להלן קישורים מהירים לכל הרשימות כ-pdf-ים:
- אביב 2022-2023:
    - 104144 - טופולוגיה | [הרצאות (טלי)](2022-2023-spring/104144l-topology.pdf)
    - 106156 - לוגיקה מתמטית | [הרצאות (שי)](2022-2023-spring/106156l-mathematical-logic.pdf)
    - 104222 - תורת ההסתברות | [הרצאות (עמנואל)](2022-2023-spring/104222l-probability-theory.pdf) | [תרגולים (נוי)](2022-2023-spring/104222r-probability-theory.pdf)
    - 104274 - תורת השדות | [הרצאות (דני)](2022-2023-spring/104274l-field-theory.pdf)
- חורף 2022-2023:
    - 104122 - תורת הפונקציות | [הרצאות (רוס)](2022-2023-winter/104122l-function-theory-aka-complex-analysis.pdf) | [תרגולים (רן)](2022-2023-winter/104122r-function-theory-aka-complex-analysis.pdf)
    - 104165 - פונקציות ממשיות | [הרצאות (לירן)](2022-2023-winter/104165l-real-functions.pdf) | [תרגולים (אלון)](2022-2023-winter//104165r-real-functions.pdf)
    - 104279 - מבוא לחוגים ושדות | [הרצאות (מיכה)](2022-2023-winter/104279l-introduction-to-rings-and-fields.pdf) | [תרגולים (סתיו)](2022-2023-winter/104279r-introduction-to-rings-and-fields.pdf)


רשימות נוספות:
- תורת המספרים בכיף ובהנאה (מועדון מתמטי 11.1.23) (ד"ר אדם דור-און (אוניברסיטת חיפה)) [רשימות](mathametics-club-number-theory-for-fun.pdf)


# המלצה: [אייפד / אייפון](https://apps.apple.com/app/apple-store/id896694807),  [אנדרואיד](https://play.google.com/store/apps/details?id=com.manichord.mgit) (או פשוט [git](https://git-scm.com/))
התקנה (חד פעמית): clone דרך גיט או דרך ה-ui של האפליקציות לעיל
```bash
git clone https://gitlab.com/open-course-notes/open-course-notes.git
```
וזהו!
<br/>כל פעם שרוצות/ים לעדכן, למשל אחרי שיעור נוסף או תיקון (באופן כללי אין סיבה לא לעדכן כל הזמן):
```bash
git pull
```
(בתוך ה-clone או דרך התפריט של האפליקציות).

# טיפ של אלופות/ים
ניתן לעקוב עם RSS אחרי השינויים כאן!
<br/>
לא צריך לשאול בקבוצות מתי / איפה יש רשימות עדכניות!
<br/>
[סרטון הסבר (אנימציה 4 דקות)](https://invidio.xamh.de/watch?v=6HNUqDL-pI8)
<br/>
כל מה שדרוש הוא להוסיף את feed הפעילות:
[gitlab main commits feed](https://gitlab.com/open-course-notes/open-course-notes/-/commits/main?feed_token=jQuRvJz8WzsWe5Adi6jo&format=atom)
לקורא ה-RSS האהוב עליך, למשל
[Feedly](https://feedly.com)
[Android](https://play.google.com/store/apps/details?id=com.devhd.feedly)
[iOS / ipadOS](https://apps.apple.com/us/app/feedly-smart-news-reader/id396069556)
(פשוט להעתיק את הקישור ולהוסיף כ-feed).

המלצה חמה נוספת, לעקוב כאן במקום:
[sourcehut main commits feed](https://git.sr.ht/~giladwo/free-course-notes/log/main/rss.xml)
(בדרך כלל מתעדכן מוקדם יותר).


# שאלות, תיקונים, הערות, הארות והצעות לשיפור
תמיד מוזמנים/ות לפתוח MR או ליצור קשר בכל אופן אחר :slight_smile:


# הגדרות LyX
הרשימות נערכות עם ההגדרות הנמצאות [כאן](https://gitlab.com/open-course-notes/lyx-config)


# מוטיבציה
- למה הרשימות פתוחות? [Learn in public](https://www.swyx.io/learn-in-public/)
- למה הרשימות חופשיות?
    - [לאפשר למשתמש/ת לבחור](https://nora.codes/post/a-story-about-my-personal-trainer/)
    - [What is free software? (GNU)](https://www.gnu.org/philosophy/free-sw.en.html)
    - [What is free software? (FSF)](https://www.fsf.org/about/what-is-free-software)
- מה ולמה RSS? כי [It's time to get back to RSS](https://danielmiessler.com/blog/its-time-to-get-back-into-rss/)
