#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass my-heb-article
\begin_preamble
\usepackage{enumitem}
\setlist[itemize,1]{label={\fontfamily{cmr}\fontencoding{T1}\selectfont\textbullet}}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language hebrew
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "Simple CLM"
\font_sans "default" "FreeSans"
\font_typewriter "default" "FreeMono"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts true
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
תורת המספרים בכיף ובהנאה
\end_layout

\begin_layout Author
ד
\begin_inset Quotes erd
\end_inset

ר אדם דור-און )אוניברסיטת חיפה(
\end_layout

\begin_layout Theorem
לכל 
\begin_inset Formula $n\ge3$
\end_inset

 אין 
\begin_inset Formula $a,b,c>0$
\end_inset

 שלמים כך ש-
\begin_inset Formula $a^{n}+b^{b}=c^{n}$
\end_inset


\end_layout

\begin_layout Theorem
\begin_inset Formula $n=2$
\end_inset

: 
\begin_inset Formula $3^{2}+4^{2}=5^{2}$
\end_inset


\end_layout

\begin_layout Theorem
\begin_inset Formula $n=4$
\end_inset

: פרמה הוכיח
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Claim
מספיק להוכיח ל-
\begin_inset Formula $3\le p=n$
\end_inset

 ראשוני.
\end_layout

\begin_layout Claim
נניח ש-
\begin_inset Formula $n$
\end_inset

 פריק אז אפשר לכתוב 
\begin_inset Formula $n=4\cdot m$
\end_inset

 או 
\begin_inset Formula $n=p\cdot m$
\end_inset

 לראשוני 
\begin_inset Formula $p\ge3$
\end_inset

.
\end_layout

\begin_layout Claim
נניח 
\begin_inset Formula $a,b,c$
\end_inset

 פתרון 
\begin_inset Formula $a^{n}+b^{n}=c^{n}$
\end_inset

 אז 
\begin_inset Formula $n=p\cdot m$
\end_inset

 כלומר 
\begin_inset Formula $\left(a^{m}\right)^{p}=\left(b^{m}\right)^{p}+\left(c^{m}\right)^{p}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Theorem
המשפט היסודי של האריתמטיקה
\end_layout

\begin_layout Theorem
לכל 
\begin_inset Formula $n\ge2$
\end_inset

 יש ראשיניים שונים 
\begin_inset Formula $p_{1},\dots,p_{k}$
\end_inset

 וחזקות 
\begin_inset Formula $\alpha_{1},\dots,\alpha_{k}$
\end_inset

 כך ש-
\begin_inset Formula $n=p_{1}^{\alpha_{1}}\cdot\dots\cdot p_{k}^{\alpha_{k}}$
\end_inset

.
\end_layout

\begin_layout Theorem
אם 
\begin_inset Formula $p_{1}<p_{2}<\dots<p_{k}$
\end_inset

 ו-
\begin_inset Formula $q_{1}<q_{2}<\dots<q_{l}$
\end_inset

 ראשוניים כך ש-
\begin_inset Formula $n=q_{1}^{\beta_{1}}\cdot\dots\cdot q_{l}^{\beta_{l}}$
\end_inset

, 
\begin_inset Formula $\beta_{q},\dots,\beta_{l}\ge1$
\end_inset

 אז
\end_layout

\begin_deeper
\begin_layout Enumerate
\begin_inset Formula $n=l$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $p_{i}=q_{i}$
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $\alpha_{i}=\beta_{i}$
\end_inset


\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Standard
נסמן 
\begin_inset Formula $\nu_{p}\left(n\right)=\alpha$
\end_inset

 הגדולה ביותר כך ש-
\begin_inset Formula $p^{\alpha}\mid n$
\end_inset

 ל-
\begin_inset Formula $p$
\end_inset

 ראשוני.
\end_layout

\begin_layout Standard
נרצה להראות ש-
\begin_inset Formula $p\nmid\frac{N!}{p}$
\end_inset

.
\end_layout

\begin_layout Exercise
לכל 
\begin_inset Formula $N\ge2$
\end_inset

 
\begin_inset Formula $\sum_{n=1}^{N}\frac{1}{n}$
\end_inset

 לא שלם.
\end_layout

\begin_layout Solution
\begin_inset Formula $\sum_{n=1}^{N}\frac{1}{n}=\frac{\sum_{n=1}^{N}\frac{N!}{n}}{N!}$
\end_inset


\end_layout

\begin_layout Solution
נקח 
\begin_inset Formula $p$
\end_inset

 הגדול ביותר כך ש-
\begin_inset Formula $p\le N$
\end_inset


\end_layout

\begin_layout Solution
אז 
\begin_inset Formula $p\mid N!$
\end_inset

 ו-
\begin_inset Formula $p\mid\frac{N!}{k}$
\end_inset

 כאשר 
\begin_inset Formula $N\ge k\ne p$
\end_inset


\end_layout

\begin_layout Solution
אם כן אז 
\begin_inset Formula $p^{2}\mid N!$
\end_inset

 ולכן בהכרח 
\begin_inset Formula $2p\le N$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Paragraph
השערת ברטרנד )
\lang english
BP 1845, Chevyshev 1852
\lang hebrew
(
\end_layout

\begin_layout Standard
לכל 
\begin_inset Formula $n\ge2$
\end_inset

 יש ראשוני 
\begin_inset Formula $q\in\left(n,2n\right)$
\end_inset

.
\end_layout

\begin_layout Standard
אז יש 
\begin_inset Formula $p<q<2p\le N$
\end_inset

 בסתירה למקסימליות 
\begin_inset Formula $p$
\end_inset

.
\end_layout

\begin_layout Standard
ניווכח שההשערה נכונה עד
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
468
\numeric off
: 
\begin_inset Formula $3,5,7,13,23,43,83,163,317,631$
\end_inset


\end_layout

\begin_layout Definition
ל-
\begin_inset Formula $x>0$
\end_inset

 ממשי
\lang english

\begin_inset Formula 
\[
\pi\left(x\right)=\left|\left\{ p\middle|1\le p\le x,\text{\ensuremath{p} is prime}\right\} \right|
\]

\end_inset


\end_layout

\begin_layout Theorem
משפט המספרים הראשוניים
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
PNT
\end_layout

\begin_layout Theorem
)
\lang english
Hadamoard / Parosin 1896
\lang hebrew
(
\end_layout

\begin_layout Theorem
\begin_inset Formula 
\[
\lim_{x\to\infty}\frac{\pi\left(x\right)\log\left(x\right)}{x}=1
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Claim
.

\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
PNT
\lang hebrew
 
\begin_inset Formula $\Longleftarrow$
\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
BP
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
אסימפטוטית
\end_layout

\begin_layout Claim
לכל 
\begin_inset Formula $\epsilon>0$
\end_inset

 יש 
\begin_inset Formula $n_{0}$
\end_inset

 כך שלכל 
\begin_inset Formula $n\ge n_{0}$
\end_inset

 בקטע 
\begin_inset Formula $\left(n,n\left(1+\epsilon\right)\right)$
\end_inset

 יש ראשוני.
\end_layout

\begin_layout Proof
\begin_inset Formula 
\begin{align*}
\pi\left(n\left(1+\epsilon\right)\right)-\pi\left(n\right) & \underset{n\to\infty}{\sim}\frac{n\left(1+\epsilon\right)}{\log\left(n\left(1+\epsilon\right)\right)}-\frac{n}{\log\left(n\right)}\\
 & =\frac{n\left(1+\epsilon\right)\log\left(n\right)-n\left(\log\left(n\right)+\log\left(1+\epsilon\right)\right)}{\log\left(n\right)\log\left(n\left(n+\epsilon\right)\right)}\\
 & =\frac{\epsilon n\log\left(n\right)-n\log\left(1+\epsilon\right)}{\log\left(n\right)\log\left(n\left(1+\epsilon\right)\right)}\\
 & \to\infty
\end{align*}

\end_inset


\end_layout

\begin_layout Proof
בפרט יש 
\begin_inset Formula $n_{0}$
\end_inset

 כך שלכל 
\begin_inset Formula $n\ge n_{0}$
\end_inset

 
\begin_inset Formula $\pi\left(n\left(1+\epsilon\right)\right)-\pi\left(n\right)\ge1$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Paragraph
השערת לג'נדר
\end_layout

\begin_layout Standard
יש ראשוני בקטע 
\begin_inset Formula $\left(n^{2},\left(n+1\right)^{2}\right)$
\end_inset

.
\end_layout

\begin_layout Standard
כן ידוע שיש 
\begin_inset Formula $\left(n^{3},\left(n+1\right)^{3}\right)$
\end_inset

 )
\lang english
Ingham
\lang hebrew
(
\end_layout

\begin_layout Theorem
לכל 
\begin_inset Formula $n\ge2$
\end_inset


\begin_inset Formula 
\[
\frac{1}{6}\frac{n}{\log\left(n\right)}<\pi\left(n\right)<6\cdot\frac{n}{\log\left(n\right)}
\]

\end_inset


\end_layout

\begin_layout Theorem
וזה נותן ראשוני בקטע 
\begin_inset Formula $\left(n,18n\left(1+\epsilon\right)\right)$
\end_inset

 לכל 
\begin_inset Formula $n\ge2$
\end_inset

.
\end_layout

\begin_layout Paragraph
נוסחאות לגנדר
\end_layout

\begin_layout Standard
לל 
\begin_inset Formula $n\ge2$
\end_inset

 וראשוני 
\begin_inset Formula $p$
\end_inset

 מתקיים
\begin_inset Formula 
\[
\nu_{p}\left(n!\right)=\sum_{i=1}^{\infty}\left\lfloor \frac{n}{p^{i}}\right\rfloor =\sum_{i=1}^{\log_{p}\left(N_{d}\right)}\left\lfloor \frac{n}{p^{i}}\right\rfloor 
\]

\end_inset


\end_layout

\begin_layout Proof
אם 
\begin_inset Formula $p>n$
\end_inset

 בבירור 
\begin_inset Formula $\nu_{p}\left(n!\right)=0$
\end_inset


\end_layout

\begin_layout Proof
אז אם 
\begin_inset Formula $p\le n$
\end_inset

 יש לפחות חזקה אחת שלם ב-
\begin_inset Formula $n!$
\end_inset

.
\end_layout

\begin_layout Proof
הכפולות של 
\begin_inset Formula $p$
\end_inset

 ב-
\begin_inset Formula $\left\{ 1,\dots,n\right\} $
\end_inset

 הן 
\begin_inset Formula $p,2p,\dots,k_{1}p$
\end_inset

 כאשר 
\begin_inset Formula $k_{1}=\left\lfloor \frac{n}{p}\right\rfloor $
\end_inset


\end_layout

\begin_layout Proof
כל אחת מוסיפה 
\begin_inset Formula $1$
\end_inset

 ל-
\begin_inset Formula $\alpha=\nu_{p}\left(n!\right)$
\end_inset


\end_layout

\begin_layout Proof
הכפולות של 
\begin_inset Formula $p^{2}$
\end_inset

 ב-
\begin_inset Formula $\left\{ 1,\dots,n\right\} $
\end_inset

 הן 
\begin_inset Formula $p^{2},2p^{2},\dots,k_{2}p^{2}$
\end_inset

 
\begin_inset Formula $k_{2}=\left\lfloor \frac{n}{p^{2}}\right\rfloor $
\end_inset

 כל אחת מוסיפה עוד
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
1
\family roman
\series medium
\shape up
\size normal
\emph off
\numeric off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
ל-
\begin_inset Formula $\alpha$
\end_inset


\end_layout

\begin_layout Proof
ולכן 
\begin_inset Formula 
\[
\alpha=\nu_{p}\left(n!\right)=\sum_{i=1}^{\infty}\left\lfloor \frac{n}{p^{i}}\right\rfloor 
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Lemma
לכל 
\begin_inset Formula $n>0$
\end_inset

 שלם
\begin_inset Formula 
\[
\frac{4^{n}}{2n}<\binom{2n}{n}
\]

\end_inset


\end_layout

\begin_layout Lemma
תרגיל
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Lemma
לכל 
\begin_inset Formula $n\ge2$
\end_inset

 ו-
\begin_inset Formula $p$
\end_inset

 ראשוני נסמן 
\begin_inset Formula $R\left(1,p\right)=\nu_{p}\left(\binom{2n}{n}\right)$
\end_inset

 אז 
\begin_inset Formula $R\left(n,p\right)\le2n$
\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula 
\[
\nu_{p}\left(\binom{2n}{n}\right)=\nu_{p}\left(\left(2n\right)!\right)-\underbrace{\nu_{p}\left(\left(n!\right)^{2}\right)}_{=2\nu_{p}\left(n!\right)}=\sum_{i=1}^{\left\lfloor \log_{p}\left(2n\right)\right\rfloor }\underbrace{\left\lfloor \frac{2n}{p^{i}}\right\rfloor -2\left\lfloor\frac{n}{p^{i}}\right\rfloor}_{\le1}\le\log_{p}\left(2n\right)
\]

\end_inset


\end_layout

\begin_layout Proof
ואז 
\begin_inset Formula $p^{R\left(n,p\right)}\le2n$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Lemma
עבור ראשוני אי זוגי עם 
\begin_inset Formula $\frac{2n}{3}<p\le n$
\end_inset

 מתקיים 
\begin_inset Formula $\nu_{p}\left(\binom{2n}{n}\right)=0$
\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula $3p>2n$
\end_inset

 אז 
\begin_inset Formula $\nu_{p}\left(\left(2n\right)!\right)=2$
\end_inset

 ו-
\begin_inset Formula $2p\ge\frac{3}{2}p>2$
\end_inset

 ולכן 
\begin_inset Formula $\nu_{p}\left(n!\right)=1$
\end_inset

 ואז 
\begin_inset Formula $\nu_{p}\left(\binom{2n}{n}\right)=\nu_{p}\left(\left(2n\right)!\right)-\nu_{p}\left(\left(n!\right)^{2}\right)=0$
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Lemma
נסמן לכל 
\begin_inset Formula $x>0$
\end_inset

 
\begin_inset Formula $x\#=\prod_{p\le x,\text{\ensuremath{p} is prime}}p$
\end_inset


\end_layout

\begin_layout Lemma
אז לכל 
\begin_inset Formula $x\ge3$
\end_inset

 מתקיים 
\begin_inset Formula $x\#\le2^{2x-3}$
\end_inset


\end_layout

\begin_layout Lemma
נשים לב ש-
\begin_inset Formula $x\#=p_{\pi\left(x\right)}\#$
\end_inset


\end_layout

\begin_layout Lemma
בפרט, אם 
\begin_inset Formula $x=n$
\end_inset

 שלם ופריק אז 
\begin_inset Formula $x\#=\left(n-1\right)\#$
\end_inset


\end_layout

\begin_layout Proof
מהנחת אינדוקציה שלמה, 
\begin_inset Formula $x=n$
\end_inset

 ראשוני ובהכרח אי-זוגי.
\end_layout

\begin_layout Proof
נכתוב 
\begin_inset Formula $n=2m-1$
\end_inset

 וכיוון וכל ראשוני בקטע 
\begin_inset Formula $\left(m,2m\right)$
\end_inset

 מופיע במונה של 
\begin_inset Formula $\binom{2m-1}{m}$
\end_inset

 ולא במכנה שלו
\begin_inset Formula 
\[
\frac{\left(2m-1\right)\#}{m\#}\le\binom{2m-1}{m}=\frac{1}{2}\left(\binom{2m-1}{m-1}+\binom{2m-1}{m}\right)<\frac{1}{2}\left(1+1\right)^{2m-1}=2^{2m-2}
\]

\end_inset


\end_layout

\begin_layout Proof
מהנחת האינדוקציה
\begin_inset Formula 
\[
n\#=\left(2m-1\right)\$\le m\#\cdot2^{2m-2}<2^{2m-3}\cdot2^{2m-2}=2^{4m-5}=2^{2n-3}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Proof
הוכחת
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
BP
\lang hebrew
:
\end_layout

\begin_layout Proof
נניח בשלילה 
\begin_inset Formula $n$
\end_inset

 הוא כזה כך שאין ראשוני בקטה 
\begin_inset Formula $\left(n,2n\right)$
\end_inset

.
\end_layout

\begin_layout Proof
ממה שראינו 
\begin_inset Formula $n\ge468$
\end_inset

.
\end_layout

\begin_layout Proof
ידוע לנו שאין ראשוני 
\begin_inset Formula $p\mid\binom{2n}{n}$
\end_inset

 כך ש-
\end_layout

\begin_layout Proof
\begin_inset Formula $p>2n$
\end_inset


\end_layout

\begin_layout Proof
\begin_inset Formula $p=2n$
\end_inset

 )
\begin_inset Formula $p$
\end_inset

 ראשוני(
\end_layout

\begin_layout Proof
\begin_inset Formula $n<p<2n$
\end_inset

 )הנחת שלילה(
\end_layout

\begin_layout Proof
\begin_inset Formula $\frac{2n}{3}<p\le n$
\end_inset

 )למה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
3
\numeric off
(
\end_layout

\begin_layout Proof
אז אם 
\begin_inset Formula $p\mid\binom{2n}{n}$
\end_inset

 אז 
\begin_inset Formula $p\le\frac{2n}{3}$
\end_inset

 כאשר 
\begin_inset Formula $p>\sqrt{2n}$
\end_inset

 בהכרח 
\begin_inset Formula $\nu_{p}\left(\binom{2n}{n}\right)\le1$
\end_inset

 כי אחרת 
\begin_inset Formula $2n<p^{2}\mid\left(2n\right)!$
\end_inset

 מלמה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
2
\numeric off
.
\end_layout

\begin_layout Proof
לכל 
\begin_inset Formula $p\le\sqrt{2n}$
\end_inset

 
\begin_inset Formula $p^{\nu_{p}\left(\binom{2n}{n}\right)}\le2n$
\end_inset


\lang english

\begin_inset Formula 
\begin{align*}
\frac{4^{n}}{2n}\underset{\text{lemma 1}}{\le}\binom{2n}{n} & =\left(\prod_{p\le\sqrt{2n}}p^{\nu_{p}\left(\binom{2n}{n}\right)}\right)\cdot\prod_{\sqrt{2n}<p\le\frac{2n}{3}}p^{\nu_{p}\left(\binom{2n}{n}\right)}\\
 & \le\left(2n\right)^{\sqrt{2n}}\cdot\prod_{1\le p\le\frac{2n}{3}}\\
 & =\left(2n\right)^{\sqrt{2n}}\cdot\left(\frac{2n}{3}\right)\#\\
 & \le\left(2n\right)^{\sqrt{2n}}\cdot4^{\frac{2n}{3}}
\end{align*}

\end_inset


\end_layout

\begin_layout Proof
נקח לוגריתם ונקבל
\begin_inset Formula 
\[
n\log\left(4\right)-\log\left(2n\right)\le\sqrt{2n}\cdot\log\left(2n\right)+\frac{2n}{3}\log\left(4\right)
\]

\end_inset


\begin_inset Formula 
\[
\Longrightarrow\frac{n}{3}\log\left(4\right)\le\left(\sqrt{2n}+1\right)\log\left(2n\right)
\]

\end_inset


\end_layout

\begin_layout Proof
ל-
\begin_inset Formula $n=467$
\end_inset

 אי השיוויון נכון ול-
\begin_inset Formula $468=n$
\end_inset

 ואילך לא נכון.
\end_layout

\begin_layout Proof
סתירה.
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset line
LatexCommand rule
offset "0.5ex"
width "100col%"
height "1pt"

\end_inset


\end_layout

\begin_layout Standard
נסמן
\lang english

\begin_inset Formula 
\[
\varphi\left(n\right)=\left|\left\{ \text{\ensuremath{k} is integer}\middle|1\le k\le n,\text{there is no prime \ensuremath{p} such that \ensuremath{\gcd\left(p,n\right)=1} and \ensuremath{p\mid n}}\right\} \right|
\]

\end_inset


\lang hebrew

\begin_inset Formula 
\[
\varphi\left(n\right)=\prod_{i=1}^{k}\left(p_{i}^{\alpha_{i}}-p_{i}^{\alpha_{i}-1}\right)\qquad n=p_{1}^{\alpha_{i}}\cdot\dots\cdot p_{k}^{\alpha_{k}}
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula $\varphi\left(p\right)=p-1\mid p-1$
\end_inset


\end_layout

\begin_layout Definition
מספר פריק 
\begin_inset Formula $n$
\end_inset

 נקרא למר )
\lang english
Lehmer
\lang hebrew
( אם 
\begin_inset Formula $\varphi\left(n\right)\mid n-1$
\end_inset

 למר
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
1932
\numeric off
 
\begin_inset Formula $n$
\end_inset

 ראשוני אם ורק אם 
\begin_inset Formula $\varphi\left(n\right)\mid n-1$
\end_inset


\end_layout

\begin_layout Theorem
)
\lang english
Luca & Pomerance 2011
\lang hebrew
(
\end_layout

\begin_layout Theorem
לכל 
\begin_inset Formula $\epsilon>0$
\end_inset

 יש 
\begin_inset Formula $C>0$
\end_inset

 כך ש-
\begin_inset Formula 
\[
L\left(x\right)\le C\cdot\frac{x^{\frac{1}{2}}}{\left(\log\left(x\right)\right)^{\frac{1}{2}}}
\]

\end_inset


\end_layout

\end_body
\end_document
